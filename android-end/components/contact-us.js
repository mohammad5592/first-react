import React , {Component} from 'react';
import{
    TextInput,
    TouchableOpacity,
    View,
    StyleSheet,
    Text,
    ScrollView,
    Dimensions
}from 'react-native';

export default class ContactUsScreen extends Component{
    render(){
        return(
            <ScrollView>
                <View style={[styles.view1,{height:(Dimensions.get('window').height-101)}]} >
                    <View style={styles.view11} >
                        <TextInput
                            style={styles.textInput1}
                            placeholder='موضوع'
                        />
                        <TouchableOpacity
                            style={styles.touchableOpacity}
                        >
                        <Text>ارسال</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.view12} >
                        <TextInput
                            style={styles.textInput2}
                            placeholder='قضیه چیه'
                            multiline={true}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    view1:{
        margin:10,
        elevation: 8,
        borderRadius:25,
        borderWidth:1,
        borderColor:'blue',
        borderRadius:10,
        backgroundColor:'white',
        padding:10
    },

    view11:{
        flex:10,
        flexDirection:'row',
    },

    view12:{
        flex:90,
    },

    textInput1:{
        flex:80,
        borderWidth:1,
        elevation: 8,
        borderColor:'green',
        borderRadius:10,
        marginTop:5,
        marginLeft:5,
        marginRight:5,
        backgroundColor:'white',
        padding:15
    },

    textInput2:{
        flex:80,
        borderWidth:1,
        elevation: 8,
        borderColor:'yellow',
        borderRadius:10,
        margin:8,
        backgroundColor:'white',
        padding:15
    },

    touchableOpacity:{
        flex:20,
        elevation: 8,
        borderWidth:1,
        borderColor:'red',
        borderRadius:10,
        marginRight:5,
        marginTop:5,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'white'
    }
});
