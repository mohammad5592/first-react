import React , {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';

export default class DetailScreen extends Component{
    render(){
        return(
            <View style={styles.view} >
                <View style={[styles.view,{backgroundColor:'green',justifyContent:'flex-end',marginTop:50}]} >
                    <AllahoAkbar />
                </View>
                <View style={[styles.view,{backgroundColor:'white',justifyContent:'center'}]} >
                <Image
                    style={{width: 100, height: 100}}
                    source={{uri:'http://limoographic.com/wp-content/uploads/2017/02/IRI-logo-LimooGraphic.jpg'}}
                />
                </View>
                <View style={[styles.view,{backgroundColor:'red',marginBottom:50}]} >
                    <AllahoAkbar />
                </View>
            </View>
        );
    }
}

class AllahoAkbar extends Component{
    render(){
        return(
            <View style={styles.txtContainer} >
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
                <Text style={styles.txt} >الله اکبر</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view:{
        flex:1,
        alignItems:'center'
    },

    txtContainer:{
        flexDirection:'row',
        justifyContent:'space-around',
    },

    txt:{
        color:'white',
        fontSize:10,
    }
});
