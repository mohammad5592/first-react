import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
    ScrollView,
} from "react-native";

export default class HomeScreen extends Component {
    render() {
        return (
            <ScrollView>

                <View style={styles.view} >

                    <NavButton text='بازی جدید' navigation={this.props.navigation} address='Quiz' />
                    <NavButton text='جزییات' navigation={this.props.navigation} address='Detail' />

                </View>

                <View style={styles.view} >

                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    >
                        <TouchableOpacity
                            onPress={
                                () => this.props.navigation.navigate('Quiz')
                            }
                        >
                            <Image
                                source={require('../images/flow1.jpeg')}
                                style={styles.img}
                            />
                            <Text style={styles.overtext} > بازی جدید</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () => this.props.navigation.navigate('Detail')
                            }
                        >
                            <Image
                                source={require('../images/flow2.jpeg')}
                                style={styles.img}
                            />
                            <Text style={styles.overtext} >جزییات</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={
                                () => this.props.navigation.navigate('ContactUs')
                            }
                        >
                            <Image
                                source={require('../images/flow3.jpeg')}
                                style={styles.img}
                            />
                            <Text style={styles.overtext} >تماس با ما</Text>
                        </TouchableOpacity>
                    </ScrollView>

                </View>

                <View style={styles.view} >

                    <Image
                        source={require('../images/flow1.jpeg')}
                        style={styles.img2}
                    />
                    <Image
                        source={require('../images/flow2.jpeg')}
                        style={styles.img2}
                    />
                    <Image
                        source={require('../images/flow3.jpeg')}
                        style={styles.img2}
                    />

                </View>

                <View style={styles.view2}>

                    <Image
                        source={require('../images/flow1.jpeg')}
                        style={styles.img3}
                    />
                    <Image
                        source={require('../images/flow2.jpeg')}
                        style={styles.img3}
                    />
                    <Image
                        source={require('../images/flow3.jpeg')}
                        style={styles.img3}
                    />

                </View>

                <View style={styles.view}>

                    <NavButton text='تماس با ما' navigation={this.props.navigation} address='ContactUs' />

                </View>

            </ScrollView>
        );
    }
}

class NavButton extends Component {
    render() {
        return (
            <TouchableOpacity
                style={styles.bu}
                onPress={
                    () => this.props.navigation.navigate(this.props.address)
                }
            >
                <Text style={styles.buText} >{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        alignItems: 'center'
    },

    view2: {
        flex: 1,
        flexDirection:'row',
        alignItems: 'center'
    },

    bu: {
        alignItems: 'center',
        elevation: 10,
        justifyContent: 'center',
        height: 50,
        width: 125,
        borderWidth: 1,
        borderColor: 'blue',
        borderRadius: 50,
        margin: 10,
        backgroundColor: 'white'
    },

    buText: {
        color: 'blue',
        fontSize: 20
    },

    overtext:{
        position:'absolute',
        top:110,
        left:130,
        color:'#fea634',
        fontSize:30
    },

    img: {
        height: 150,
        width: 250,
        borderRadius: 20,
        margin: 10
    },

    img2: {
        height: 200,
        width: 320,
        borderRadius: 20,
        margin: 10
    },

    img3: {
        height: 100,
        flex:1,
        borderRadius: 20,
        marginHorizontal: 5
    }
});
