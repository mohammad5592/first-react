import React , {Component} from 'react';
import {
    View,
    StyleSheet,
    Text,
    Dimensions,
    TouchableHighlight} from 'react-native';
import {getDataFromServer,sendDataToServer} from '../network/fetch-data.js';

/*کامپوننت ها*/

export default class QuizScreen extends Component{

    constructor(){
        super();
        this.state = {
            qsource:'',
            asource:[],
        };
    }

    componentDidMount(){
        this.refreshData();
    }

    refreshData = () => {
        getDataFromServer().then((data)=>{
            this.setState({qsource:data[0]});
            this.setState({asource:data[1]});
        }).catch((error)=>{
            this.setState({qsource:''});
            this.setState({asource:[]});
        })
    }

    getRndInteger(min=1, max=4) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

    render(){
        return (
            <View style={styles.container} >
                <QuestionPart questionsource={this.state.qsource} />
                <BottomPart bottomsource={this.state.asource} />
            </View>
        );
    }
}

class QuestionPart extends Component{
    render(){
        return (
            <View style={styles.questionContainer}>
                <View style={styles.question}>
                    <Text style={styles.questionText}>{this.props.questionsource}</Text>
                </View>
            </View>
        );
    }
}

class BottomPart extends Component{
    render(){
        return (
            <View style={styles.bottomContainer}>
                <AnswerContainer answerscontainersource={this.props.bottomsource} />
                <Hint />
            </View>
        );
    }
}

class AnswerContainer extends Component{
    render(){
        return(
            <View style={styles.answerContainer}>
                <AswerItem answersource={this.props.answerscontainersource[0]} />
                <AswerItem answersource={this.props.answerscontainersource[1]} />
                <AswerItem answersource={this.props.answerscontainersource[2]} />
                <AswerItem answersource={this.props.answerscontainersource[3]} />
            </View>
        );
    }
}

class Hint extends Component{
    render(){
        return(
            <View style={styles.hint}>

            </View>
        );
    }
}

class AswerItem extends Component{

    _onPress = ()=>{
        sendDataToServer(`${this.props.answersource}`);
    }
    
    render(){
        return(
            <TouchableHighlight
                style={styles.answer}
                underlayColor='rgb(252, 221, 57)'
                activeOpacity={1}
                onPress={this._onPress}
             >
                <Text style={styles.answerText} >{this.props.answersource}</Text>
            </TouchableHighlight>
        );
    }
}

/*ظاهر برنامه */

const styles = StyleSheet.create({

    screenSize:{
        height:Dimensions.get('window').height-25,
        width:Dimensions.get('window').width
    },

    screenHeight:{
        height:Dimensions.get('window').height-25,
    },

    screenWidth:{
        width:Dimensions.get('window').width
    },

    container:{
        flex:1,
        backgroundColor:'rgb(98, 133, 204)'
    },

    questionContainer:{
        flex:50,
    },

    question:{
        flex:1,
        backgroundColor:'rgb(54, 198, 159)',
        margin:10,
        borderRadius:20,
        justifyContent:'center',
    },

    questionText:{
        fontSize:25,
        color:'rgb(221, 255, 243)',
        marginHorizontal:5,
        textAlign:'center'
    },

    bottomContainer:{
        flex:50,
        flexDirection:'row',
        marginBottom:10
    },

    answerContainer:{
        flex:90,
        flexDirection:'column',
    },

    answer:{
        flex:1,
        marginLeft:10,
        marginVertical:2,
        backgroundColor:'rgb(51, 70, 200)',
        justifyContent:'center'
    },

    answerText:{
        fontSize:15,
        fontWeight:'bold',
        color:'rgb(221, 255, 243)',
        marginHorizontal:15,
    },

    hint:{
        flex:10,
        backgroundColor:'rgb(241, 86, 105)',
        marginRight:10,
        borderTopRightRadius:10,
        borderBottomRightRadius:10
    },
});
