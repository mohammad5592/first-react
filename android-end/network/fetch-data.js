import React , {Component} from 'react';
import { returnStatement } from '@babel/types';
const viewData = 'http://192.168.1.6/';

async function getDataFromServer(){
    try{
        let response = await fetch(viewData,{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
            });
        let jResponse = await response.json();
        return jResponse;
    }catch(error){
        alert(`${error}`);
    }
}

async function sendDataToServer(data){
    try{
        let response = await fetch(viewData,{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify(data)
            });
        let jResponse = await response.json();
        return jResponse;
    }catch(error){
        alert(`${error}`);
    }
}
export {getDataFromServer};
export {sendDataToServer};
