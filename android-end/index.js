import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import HomeScreen from './components/home.js';
import QuizScreen from './components/quiz.js';
import DetailScreen from './components/detail.js';
import ContactUsScreen from './components/contact-us.js';

import { createStackNavigator, createAppContainer } from "react-navigation";

const App = createStackNavigator({
    Home:{
        screen:HomeScreen,
        navigationOptions:{
            title:'خانه'
        }
    },
    Quiz:{
        screen:QuizScreen,
        navigationOptions:{
            title:'بازی'
        }
    },
    Detail:{
        screen:DetailScreen,
        navigationOptions:{
            title:'جزییات'
        }
    },
    ContactUs:{
        screen:ContactUsScreen,
        navigationOptions:{
            title:'تماس با ما'
        }
    }
});

AppRegistry.registerComponent(appName, () => createAppContainer(App));
