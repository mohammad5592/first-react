<?php
$hostname = 'localhost';
$username = 'admin';
$password = '****';
$dbname = 'mobin';


try {
    $connection = new PDO("mysql:host=" . $hostname . ";dbname=" . $dbname . ";charset=utf8;", $username, $password);
    $num = 0;
    $q = $connection->prepare("SELECT *FROM movie AS r1 JOIN(SELECT CEIL(RAND() *(SELECT MAX(id)FROM movie)) AS id)AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 1");
    $q->execute();
    $movie_id = $q->fetch(Pdo::FETCH_ASSOC);

    $q = $connection->prepare("SELECT
        p.id as n0,
        p.first_name AS n1,
        p.last_name AS n2,
        p.gender as n3,
        r.character_name AS cn
    FROM
        person p
    JOIN movie m ON
        m.id = ?
    JOIN roles r ON
        r.person_id = p.id AND
        r.role_Type = ? AND
        r.movie_id = m.id
    ORDER BY
        RAND() LIMIT 1;");


    $q->bindParam(1, $movie_id['id']);
    $q->bindValue(2, "بازیگر");

    $q->execute();
    $r = $q->fetch(PDO::FETCH_ASSOC);

    if (!$r) echo $q->errorInfo();

    $q = $connection->prepare("SELECT
       DISTINCT (p.id),
    p.first_name AS n1,
    p.last_name AS n2
FROM
    person p
JOIN roles r ON
    r.person_id != ? AND r.role_Type = ? and p.gender = ? and p.id = r.person_id
ORDER BY
    RAND()
LIMIT 3;");

    $q->bindParam(1, $r['n0']);
    $q->bindValue(2, "بازیگر");
    $q->bindParam(3, $r['n3']);
    $q->execute();
    $other = $q->fetchAll(pdo::FETCH_ASSOC);
    $rr = "کدام بازیگر نقش ";
    $rr = $rr.$r['cn'];
    $rr = $rr." را در فیلم ";
    $rr = $rr.$movie_id['title'];
    $rr = $rr." ایفا کرده است؟";


    $aswers = array();
    $aswers['0'] = $other['0']['n1'] . " " . $other['0']['n2'];
    $aswers['1'] = $other['1']['n1'] . '' . $other['1']['n2'];
    $aswers['2'] = $other['2']['n1'] . " " . $other['2']['n2'];
    $aswers['3'] = $r['n1'] . " " . $r['n2'];

    $myarray = array();
    $myarray['0'] = "$rr";
    $myarray['1'] = $aswers;
    echo json_encode($myarray);

} catch (Exception $e) {
    echo "Error! " . $e->getMessage();
}
?>
